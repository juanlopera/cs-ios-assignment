//
//  HttpError.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 12/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation

enum HttpError: Error, Equatable {
    case noConnectivity
    case badRequest
    case serverError
    case unauthorized
    case serviceUnavailable
    case forbidden
    case notFound
    case genericError(rawValue: Int)
}
