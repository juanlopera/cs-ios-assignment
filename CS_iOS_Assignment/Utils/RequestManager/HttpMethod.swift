//
//  HttpMethod.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 12/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation

enum HttpMethod: String {
    case get = "GET"
}
