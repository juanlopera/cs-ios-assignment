//
//  String+Extension.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation

extension String {
    func getReleaseDate() -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterToShow = DateFormatter()
        dateFormatterToShow.dateFormat = "MMMM d, yyyy"
        guard
            let date = dateFormatterGet.date(from: self)
        else {
            return ""
        }
        return dateFormatterToShow.string(from: date)
    }
}
