//
//  UIImage+Extension.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

extension UIImage {
    static func getMovieUIImageFrom(posterPath: String) -> UIImage {
        var urlComponent = URLComponents()
        urlComponent.scheme = "https"
        urlComponent.host = "image.tmdb.org"
        urlComponent.path = "/t/p/w200\(posterPath)"
        guard let url: URL = urlComponent.url else {
            return UIImage(named: "moviePlaceholder") ?? UIImage()
        }
        if let data = try? Data(contentsOf: url) {
            if let image = UIImage(data: data) {
                return image
            }
        }
        return UIImage(named: "moviePlaceholder") ?? UIImage()
    }
}
