//
//  UIViewController+Extension.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 14/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

extension UIViewController {
    func showError(with type: ErrorViewType, reload: (() -> Void)? = nil) {
        let errorView = ErrorView(type: type)
        errorView.reloadAction = reload
        errorView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(errorView)
        
        NSLayoutConstraint.activate([errorView.topAnchor.constraint(equalTo: self.view.layoutMarginsGuide.topAnchor),
                                     errorView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
                                     errorView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
                                     errorView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)])
    }
}
