//
//  ViewConfigurationProtocol.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 12/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation

protocol ViewConfigurationProtocol: AnyObject {
    func setupViews()
    func setupViewHierarchy()
    func setupConstraints()
    func configureViews()
}

extension ViewConfigurationProtocol {
    func setupViews() {
        setupViewHierarchy()
        setupConstraints()
        configureViews()
    }
    
    func configureViews() {}
}
