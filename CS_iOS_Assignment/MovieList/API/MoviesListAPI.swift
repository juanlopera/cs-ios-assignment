//
//  MoviesListAPI.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 12/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation

protocol MoviesListAPIProtocol: AnyObject {
    func fetchMoviesPlayingNow(completionHandler: @escaping(Result<Movie, HttpError>) -> Void)
    func fetchMoviesMostPopular(page: String, completionHandler: @escaping(Result<Movie, HttpError>) -> Void)
}

final class MoviesListAPI: MoviesListAPIProtocol {
    
    // MARK: - Enum
    enum EndPoint: String {
        case playingNow = "now_playing"
        case popular = "popular"
    }
    
    // MARK: - Private Properties
    private let apiKey: String
    private let baseURL: String
    private let requestManager: RequestManager
    
    // MARK: - Internal Init
    init(apiKey: String = "55957fcf3ba81b137f8fc01ac5a31fb5",
         baseURL: String = "https://api.themoviedb.org/3/movie/",
         requestManager: RequestManager = RequestManager()) {
        self.apiKey = apiKey
        self.baseURL = baseURL
        self.requestManager = requestManager
    }
    
    // MARK: - Protocol Implementation
    func fetchMoviesPlayingNow(completionHandler: @escaping(Result<Movie, HttpError>) -> Void) {
        let securityScheme: String  = "https"
        let host: String = "api.themoviedb.org"
        let path: String = "/3/movie/\(EndPoint.playingNow.rawValue)"
        let queryItems: [URLQueryItem] = [URLQueryItem(name: "language", value: "en-US"),
                                          URLQueryItem(name: "page", value: "undefined"),
                                          URLQueryItem(name: "api_key", value: apiKey)]
        requestManager.request(with: securityScheme, host: host, path: path, queryItems: queryItems, completion: completionHandler)
    }
    
    func fetchMoviesMostPopular(page: String, completionHandler: @escaping(Result<Movie, HttpError>) -> Void) {
        let securityScheme: String  = "https"
        let host: String = "api.themoviedb.org"
        let path: String = "/3/movie/\(EndPoint.popular.rawValue)"
        let queryItems: [URLQueryItem] = [URLQueryItem(name: "language", value: "en-US"),
                                          URLQueryItem(name: "page", value: page),
                                          URLQueryItem(name: "api_key", value: apiKey)]
        requestManager.request(with: securityScheme, host: host, path: path, queryItems: queryItems, completion: completionHandler)
    }
}
