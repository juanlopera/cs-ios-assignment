//
//  MovieDates.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 12/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation

struct MovieDates: Decodable {
    var maximum: String
    var minimum: String
}
