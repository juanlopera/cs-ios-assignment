//
//  MovieSection.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation

final class MovieSection {
    // MARK: - Enum
    enum Section: String {
        case playingNow = "Playing now"
        case mostPopular = "Most Popular"
    }
    
    // MARK: - Internal Properties
    var section: Section
    var information: [MovieList]
    
    // MARK: - Internal Init
    init(section: Section, information: [MovieList]) {
        self.section = section
        self.information = information
    }
}
