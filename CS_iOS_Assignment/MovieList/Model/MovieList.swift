//
//  MovieList.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

struct MovieList {
    
    // MARK: - Internal Properties
    var id: Int
    var image: UIImage
    var title: String
    var releaseDate: String
    var duration: String
    var rating: Float
    
    // MARK: - Internal Init
    init(id: Int = 0,
         image: UIImage = UIImage(),
         title: String = "",
         releaseDate: String = "",
         duration: String = "",
         rating: Float = 0.0) {
        self.id = id
        self.image = image
        self.title = title
        self.releaseDate = releaseDate
        self.duration = duration
        self.rating = rating
    }
}
