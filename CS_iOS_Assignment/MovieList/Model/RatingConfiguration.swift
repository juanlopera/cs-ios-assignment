//
//  RatingConfiguration.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

struct RatingConfiguration {
    var trackColor: UIColor
    var progressColor: UIColor
    var rating: Float
    var animation: Bool
    
    init(trackColor: UIColor = .clear,
         progressColor: UIColor = .clear,
         rating: Float = 0.0,
         animation: Bool = false) {
        self.trackColor = trackColor
        self.progressColor = progressColor
        self.rating = rating
        self.animation = animation
    }
}
