//
//  MovieResult.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 12/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation

struct MovieResult: Decodable {
    let id: Int
    let originalTitle: String
    let posterPath: String
    let releaseDate: String
    let title: String
    let voteAverage: Float
    
    enum CodingKeys: String, CodingKey {
        case id
        case originalTitle = "original_title"
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case title
        case voteAverage = "vote_average"
    }
}
