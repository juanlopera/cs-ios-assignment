//
//  MovieListBaseView.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 12/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Lottie
import UIKit

final class MovieListBaseView: UIView {
    
    // MARK: - TypeAlias
    typealias strings = StringConstants
    typealias colors = ColorsConstants
    
    // MARK: - Private UI Properties
    private let titleLabel: UILabel = {
        let label: UILabel = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = strings.MovieList.moviesTitle
        label.font = UIFont.boldSystemFont(ofSize: 40)
        label.textColor = colors.yellowCreamCan
        label.textAlignment = .center
        return label
    }()
    
    private let moviesTableView: UITableView = {
        let tableView: UITableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.tableFooterView = UIView()
        tableView.register(PlayingNowCell.self, forCellReuseIdentifier: strings.MovieList.tableSectionPlayingNowIdentifier)
        tableView.register(MostPopularCell.self, forCellReuseIdentifier: strings.MovieList.tableSectionMostPopularIdentifier)
        return tableView
    }()
    
    private let animationView: AnimationView = {
        let animationView: AnimationView = .init(name: "movieAnimation")
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationView.loopMode = .loop
        animationView.animationSpeed = 0.5
        animationView.isHidden = true
        return animationView
    }()
    
    // MARK: - Private Properties
    private var datasourceTable: DatasourceMoviesListTable = DatasourceMoviesListTable()
    private var delegateTable: DelegateMoviesListTable = DelegateMoviesListTable()
    
    // MARK: - Internal Init
    init() {
        super.init(frame: .zero)
        setupViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - View Code
extension MovieListBaseView: ViewConfigurationProtocol {
    func setupViewHierarchy() {
        addSubview(titleLabel)
        addSubview(moviesTableView)
        addSubview(animationView)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 80),
            titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            titleLabel.heightAnchor.constraint(equalToConstant: 50),
            
            moviesTableView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 25),
            moviesTableView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            moviesTableView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            moviesTableView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            animationView.topAnchor.constraint(equalTo: self.topAnchor),
            animationView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            animationView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            animationView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    
    func configureViews() {
        backgroundColor = colors.blackMineShaft
    }
}

// MARK: - Internal Function
extension MovieListBaseView {
    func set(viewModel: MoviesListViewModel) {
        DispatchQueue.main.async { [self] in
            delegateTable.viewModel = viewModel
            datasourceTable.viewModel = viewModel
            moviesTableView.dataSource = datasourceTable
            moviesTableView.delegate = delegateTable
            moviesTableView.reloadData()
        }
    }
    
    func isLoading(_ value: Bool) {
        DispatchQueue.main.async { [self] in
            if value {
                animationView.isHidden = false
                animationView.play()
            } else {
                animationView.isHidden = true
                animationView.stop()
            }
        }
    }
}
