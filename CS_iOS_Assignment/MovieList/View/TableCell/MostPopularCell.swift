//
//  MostPopularCell.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

final class MostPopularCell: UITableViewCell {
    
    // MARK: - Private UI Properties
    private let separatorView: UIView = {
        let view: UIView = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = ColorsConstants.grayTundora
        return view
    }()
    
    private let contentPosterView: UIView = {
        let view: UIView = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.borderWidth = 1
        view.layer.borderColor = ColorsConstants.graySilver.cgColor
        return view
    }()
    
    private let posterImageView: UIImageView = {
        let imageView: UIImageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    private let movieNameLabel: UILabel = {
        let label: UILabel = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = ColorsConstants.graySilver
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    private let releaseDateLabel: UILabel = {
        let label: UILabel = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = ColorsConstants.graySilver
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    // MARK: - Internal Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewCode
extension MostPopularCell: ViewConfigurationProtocol {
    func setupViewHierarchy() {
        contentView.addSubview(contentPosterView)
        contentPosterView.addSubview(posterImageView)
        contentView.addSubview(movieNameLabel)
        contentView.addSubview(releaseDateLabel)
        contentView.addSubview(separatorView)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            contentPosterView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            contentPosterView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            contentPosterView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            contentPosterView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15),
            contentPosterView.widthAnchor.constraint(equalToConstant: 70.0),
            
            posterImageView.topAnchor.constraint(equalTo: contentPosterView.topAnchor),
            posterImageView.leadingAnchor.constraint(equalTo: contentPosterView.leadingAnchor),
            posterImageView.bottomAnchor.constraint(equalTo: contentPosterView.bottomAnchor),
            posterImageView.trailingAnchor.constraint(equalTo: contentPosterView.trailingAnchor),
            
            movieNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 30),
            movieNameLabel.leadingAnchor.constraint(equalTo: contentPosterView.trailingAnchor, constant: 20),
            movieNameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -60),
            movieNameLabel.heightAnchor.constraint(equalToConstant: 18),
            
            releaseDateLabel.topAnchor.constraint(equalTo: movieNameLabel.bottomAnchor, constant: 6),
            releaseDateLabel.leadingAnchor.constraint(equalTo: contentPosterView.trailingAnchor, constant: 20),
            releaseDateLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -60),
            releaseDateLabel.heightAnchor.constraint(equalToConstant: 15),
            
            separatorView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            separatorView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: 5)
        ])
    }
    
    func configureViews() {
        contentView.backgroundColor = ColorsConstants.blackMineShaft
    }
}

// MARK: - Internal Function
extension MostPopularCell {
    func set(viewModel: MoviesListViewModel) {
        posterImageView.image = viewModel.mostPopularImage
        movieNameLabel.text = viewModel.movieName
        releaseDateLabel.text = viewModel.movieReleaseDate
        setupRatingView(with: viewModel)
    }
}

// MARK: - Private Function
extension MostPopularCell {
    private func setupRatingView(with viewModel: MoviesListViewModel) {
        let ratingFrame: CGRect = CGRect(x: contentView.frame.width - 60, y: (contentView.frame.height / 2) - 20, width: 40, height: 40)
        let ratingView: RatingCircularView = RatingCircularView(frame: ratingFrame,
                                                                progressColor: viewModel.movieRating.progressColor,
                                                                trackColor: viewModel.movieRating.trackColor,
                                                                ratingAverage: viewModel.movieRating.rating)
        let tarckValue: CGFloat = CGFloat(viewModel.movieRating.rating)
        let progressValue: CGFloat = tarckValue / 10.0
        if viewModel.movieRating.animation {
            ratingView.setRatingAnimation(with: 1.0, and: progressValue)
            viewModel.didRatingAnimation()
        } else {
            ratingView.setStrokeEnd(valueTrack: progressValue)
        }
        contentView.addSubview(ratingView)
    }
}
