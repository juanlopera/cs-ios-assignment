//
//  PlayingNowCell.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

final class PlayingNowCell: UITableViewCell {
    
    // MARK: - TypeAlias
    typealias strings = StringConstants
    
    // MARK: - Private UI Properties
    private let playingNowCollectionView: UICollectionView = {
        let flowLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 0
        let collectionView: UICollectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.register(PosterCollectionCell.self, forCellWithReuseIdentifier: strings.MovieList.collectionSectionPlayingNowIdentifier)
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    // MARK: - Private Properties
    private var datasourceCollection: DatasourcePlayingNowCollection = DatasourcePlayingNowCollection()
    private var delegateCollection: DelegatePlayingNowCollection = DelegatePlayingNowCollection()
    
    // MARK: - Internal Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewCode
extension PlayingNowCell: ViewConfigurationProtocol {
    func setupViewHierarchy() {
        contentView.addSubview(playingNowCollectionView)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            playingNowCollectionView.topAnchor.constraint(equalTo: contentView.topAnchor),
            playingNowCollectionView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            playingNowCollectionView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            playingNowCollectionView.heightAnchor.constraint(equalToConstant: 200.0)
        ])
    }
    
    func configureViews() {
        contentView.backgroundColor = .clear
    }
}

// MARK: - Internal Function
extension PlayingNowCell {
    func set(viewModel: MoviesListViewModel) {
        datasourceCollection.viewModel = viewModel
        delegateCollection.viewModel = viewModel
        playingNowCollectionView.dataSource = datasourceCollection
        playingNowCollectionView.delegate = delegateCollection
        playingNowCollectionView.reloadData()
    }
}
