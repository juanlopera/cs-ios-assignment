//
//  PosterCollectionCell.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

final class PosterCollectionCell: UICollectionViewCell {
    
    // MARK: - Private UI Properties
    private let posterImageView: UIImageView = {
        let imageView: UIImageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    // MARK: - Internal Init
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewCode
extension PosterCollectionCell: ViewConfigurationProtocol {
    func setupViewHierarchy() {
        contentView.addSubview(posterImageView)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            posterImageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            posterImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            posterImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            posterImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
    
    func configureViews() {
        contentView.backgroundColor = .clear
    }
}

// MARK: - Internal Fucntion
extension PosterCollectionCell {
    func set(viewModel: MoviesListViewModel) {
        posterImageView.image = viewModel.playingNowImage
    }
}
