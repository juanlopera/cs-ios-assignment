//
//  MovieListViewController.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 12/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

protocol MovieListViewControllerDelegate: AnyObject {
    func loadMovieSections()
    func selectMovie(with id: Int)
    func serviceError()
}

final class MovieListViewController: UIViewController {
    
    // MARK: - Private UI Properties
    private let baseView: MovieListBaseView = MovieListBaseView()
    
    // MARK: - Private Properties
    private let viewModel: MoviesListViewModel
    private let coordinator: MovieCoordinator
    // MARK: - Internal Init
    init(coordinator: MovieCoordinator,
         viewModel: MoviesListViewModel = MoviesListViewModel()) {
        self.coordinator = coordinator
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    override func loadView() {
        view = baseView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        baseView.isLoading(true)
        viewModel.fetchMovieList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
}

// MARK: - Protocol Implementation
extension MovieListViewController: MovieListViewControllerDelegate {
    func loadMovieSections() {
        baseView.isLoading(false)
        baseView.set(viewModel: viewModel)
    }
    
    func selectMovie(with id: Int) {
        coordinator.showDetailt(with: id)
    }
    
    func serviceError() {
        DispatchQueue.main.async { [self] in
            showError(with: .service) {
                viewModel.fetchMovieList()
            }
        }
    }
}
