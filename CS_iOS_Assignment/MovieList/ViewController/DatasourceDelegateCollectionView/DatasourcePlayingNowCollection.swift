//
//  DatasourcePlayingNowCollection.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

final class DatasourcePlayingNowCollection: NSObject, UICollectionViewDataSource {
    
    // MARK: - TypeAlias
    typealias strings = StringConstants
    
    //MARK: - Internal Properties
    var viewModel: MoviesListViewModel = MoviesListViewModel()
    
    // MARK: - Datasource Protocol Implementation
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRowInSectionCollection
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        viewModel.currentCollectionCellIndex = indexPath.item
        guard
            let cell: PosterCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: strings.MovieList.collectionSectionPlayingNowIdentifier, for: indexPath) as? PosterCollectionCell
        else {
            return UICollectionViewCell()
        }
        cell.set(viewModel: viewModel)
        return cell
    }
}
