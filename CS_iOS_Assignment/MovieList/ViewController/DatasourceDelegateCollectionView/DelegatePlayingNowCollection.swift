//
//  DelegatePlayingNowCollection.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

final class DelegatePlayingNowCollection: NSObject, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    //MARK: - Internal Properties
    var viewModel: MoviesListViewModel = MoviesListViewModel()
    
    // MARK: - Delegate CollectionView Implementation
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 140, height: viewModel.playingNowHeightRow)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        viewModel.cellSelected = (section: 0, cellIndex: indexPath.item)
    }
}
