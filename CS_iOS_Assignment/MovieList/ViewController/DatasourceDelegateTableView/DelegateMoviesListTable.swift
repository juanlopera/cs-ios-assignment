//
//  DelegateMoviesListTable.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 12/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

final class DelegateMoviesListTable: NSObject, UITableViewDelegate {
    
    //MARK: - Internal Properties
    var viewModel: MoviesListViewModel = MoviesListViewModel()
    
    // MARK: - Delegate TableView Implementation
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch viewModel.section {
        case .playingNow:
            return viewModel.playingNowHeightRow
        case .mostPopular:
            return viewModel.mostPopularHeightRow
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let section: String = viewModel.getSectionTitle(section: indexPath.section)
        if section == MovieSection.Section.mostPopular.rawValue {
            viewModel.cellSelected = (section: indexPath.section, cellIndex: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerFrame: CGRect = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50)
        let headerView: UIView = UIView(frame: headerFrame)
        headerView.backgroundColor = ColorsConstants.grayTundora
        let headerTitleFrame: CGRect = CGRect(x: 20, y: -12, width: headerView.frame.width, height: headerView.frame.height)
        let headerTitleLabel: UILabel = UILabel(frame: headerTitleFrame)
        headerTitleLabel.textColor = ColorsConstants.yellowCreamCan
        headerTitleLabel.text = viewModel.getSectionTitle(section: section)
        headerTitleLabel.font = UIFont.systemFont(ofSize: 15)
        headerView.addSubview(headerTitleLabel)
        return headerView
    }
}
