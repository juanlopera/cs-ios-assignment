//
//  DatasourceMoviesListTable.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 12/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

final class DatasourceMoviesListTable: NSObject, UITableViewDataSource {
    
    // MARK: - TypeAlias
    typealias strings = StringConstants
    
    //MARK: - Internal Properties
    var viewModel: MoviesListViewModel = MoviesListViewModel()
    
    // MARK: - Datasource Implementation
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSection
    }
    
    // MARK: - Datasource Protocol Implementation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.currentSection = section
        return viewModel.numberOfRowInSectionTable
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        viewModel.currentSection = indexPath.section
        viewModel.currentTableCellIndex = indexPath.row
        switch viewModel.section {
        case .playingNow:
            guard
                let cell: PlayingNowCell = tableView.dequeueReusableCell(withIdentifier: strings.MovieList.tableSectionPlayingNowIdentifier, for: indexPath) as? PlayingNowCell
            else {
                return UITableViewCell()
            }
            cell.set(viewModel: viewModel)
            return cell
        case .mostPopular:
            guard
                let cell: MostPopularCell = tableView.dequeueReusableCell(withIdentifier: strings.MovieList.tableSectionMostPopularIdentifier, for: indexPath) as? MostPopularCell
            else {
                return UITableViewCell()
            }
            cell.set(viewModel: viewModel)
            return cell
        }
    }
}
