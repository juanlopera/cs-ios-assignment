//
//  MoviesListViewModel.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 12/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

final class MoviesListViewModel {
    
    // MARK: - Private Properties
    private let api: MoviesListAPIProtocol
    private var movieSection: [MovieSection] = [MovieSection]()
    private var ratingMovie: [RatingConfiguration] = [RatingConfiguration]()
    // MARK: - Internal Properties
    var currentSection: Int = 0
    var currentTableCellIndex: Int = 0
    var currentCollectionCellIndex: Int = 0
    var cellSelected: (section: Int, cellIndex: Int) = (section: 0, cellIndex: 0) {
        didSet {
            delegate?.selectMovie(with: movieSection[cellSelected.section].information[cellSelected.cellIndex].id)
        }
    }
    
    // MARK: - Delegate
    weak var delegate: MovieListViewControllerDelegate?
    
    // MARK: - Internal Init
    init(api: MoviesListAPIProtocol = MoviesListAPI()) {
        self.api = api
        initMovieSection()
    }
    
    // MARK: - Computed Properties
    var playingNowHeightRow: CGFloat {
        return 200.0
    }
    
    var mostPopularHeightRow: CGFloat {
        130.0
    }
    
    var numberOfSection: Int {
        return movieSection.count
    }
    
    var numberOfRowInSectionTable: Int {
        guard
            movieSection.indices.contains(currentSection)
        else {
            return 0
        }
        switch movieSection[currentSection].section {
        case .playingNow:
            return 1
        case .mostPopular:
            return movieSection[currentSection].information.count
        }
    }
    
    var numberOfRowInSectionCollection: Int {
        guard movieSection.indices.contains(currentSection)
        else {
            return 0
        }
        return movieSection[currentSection].information.count
    }
    
    var section: MovieSection.Section {
        guard
            movieSection.indices.contains(currentSection)
        else {
            return .playingNow
        }
        return movieSection[currentSection].section
    }
    
    var playingNowImage: UIImage {
        guard
            movieSection.indices.contains(currentSection),
            movieSection[currentSection].information.indices.contains(currentCollectionCellIndex)
        else {
            return UIImage(named: "moviePlaceholder") ?? UIImage()
        }
        return movieSection[currentSection].information[currentCollectionCellIndex].image
    }
    
    var mostPopularImage: UIImage {
        guard
            movieSection.indices.contains(currentSection),
            movieSection[currentSection].information.indices.contains(currentTableCellIndex)
        else {
            return UIImage(named: "moviePlaceholder") ?? UIImage()
        }
        return movieSection[currentSection].information[currentTableCellIndex].image
    }
    
    var movieName: String {
        guard
            movieSection.indices.contains(currentSection),
            movieSection[currentSection].information.indices.contains(currentTableCellIndex)
        else {
            return "Not Information"
        }
        return movieSection[currentSection].information[currentTableCellIndex].title
    }
    
    var movieReleaseDate: String {
        guard
            movieSection.indices.contains(currentSection),
            movieSection[currentSection].information.indices.contains(currentTableCellIndex)
        else {
            return "Not Information"
        }
        return movieSection[currentSection].information[currentTableCellIndex].releaseDate.getReleaseDate()
    }
    
    var movieRating: RatingConfiguration {
        guard
            ratingMovie.indices.contains(currentTableCellIndex)
        else {
            return RatingConfiguration()
        }
        return ratingMovie[currentTableCellIndex]
    }
}

// MARK: - Internal Functions
extension MoviesListViewModel {
    func fetchMovieList() {
        fetchPlayingNowList()
    }
    
    func didRatingAnimation() {
        guard
            ratingMovie.indices.contains(currentTableCellIndex)
        else {
            return
        }
        ratingMovie[currentTableCellIndex].animation = false
    }
    
    func getSectionTitle(section: Int) -> String {
        guard
            movieSection.indices.contains(section)
        else {
            return "Not Information"
        }
        return movieSection[section].section.rawValue
    }
}

// MARK: - Private Function
extension MoviesListViewModel {
    private func initMovieSection() {
        movieSection.append(MovieSection(section: .playingNow, information: [MovieList]()))
        movieSection.append(MovieSection(section: .mostPopular, information: [MovieList]()))
    }
    
    private func fetchPlayingNowList() {
        api.fetchMoviesPlayingNow { [weak self] result in
            switch result {
            case .success(let moviePlayingnNow):
                self?.handlePlayingNow(response: moviePlayingnNow)
                self?.fetchMostPopular()
            case .failure(_):
                self?.delegate?.serviceError()
            }
        }
    }
    
    private func fetchMostPopular(with page: String = "1") {
        api.fetchMoviesMostPopular(page: page) { [weak self] result in
            switch result {
            case .success(let movieMostPopular):
                self?.handleMostPopular(response: movieMostPopular)
                self?.delegate?.loadMovieSections()
            case.failure(_):
                self?.delegate?.serviceError()
            }
        }
    }
    
    private func handlePlayingNow(response: Movie) {
        response.results.forEach { result in
            let posterImage: UIImage = UIImage.getMovieUIImageFrom(posterPath: result.posterPath)
            let movieList: MovieList = MovieList(id: result.id,
                                                 image: posterImage,
                                                 title: result.title)
            movieSection.first(where: { $0.section == .playingNow })?.information.append(movieList)
        }
    }
    
    private func handleMostPopular(response: Movie) {
        response.results.forEach { result in
            let posterImage: UIImage = UIImage.getMovieUIImageFrom(posterPath: result.posterPath)
            let movieList: MovieList = MovieList(id: result.id,
                                                 image: posterImage,
                                                 title: result.title,
                                                 releaseDate: result.releaseDate,
                                                 duration: "",
                                                 rating: result.voteAverage)
            movieSection.first(where: { $0.section == .mostPopular })?.information.append(movieList)
            addRating(with: movieList)
        }
    }
    
    private func addRating(with movieInformation: MovieList) {
        let ratingAverage: Float = movieInformation.rating
        var trackColor: UIColor = .clear
        var progressColor: UIColor = .clear
        if ratingAverage >= 5.0 {
            trackColor = ColorsConstants.greenEverglade
            progressColor = ColorsConstants.greenEmerald
        } else {
            trackColor = ColorsConstants.yellowWaiouru
            progressColor = ColorsConstants.yellowCreamCan
        }
        let rating: RatingConfiguration = RatingConfiguration(trackColor: trackColor,
                                                              progressColor: progressColor,
                                                              rating: ratingAverage,
                                                              animation: true)
        ratingMovie.append(rating)
    }
}
