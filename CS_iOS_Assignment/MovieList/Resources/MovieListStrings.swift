//
//  MovieListStrings.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 12/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation

extension StringConstants {
    struct MovieList {
        static let collectionSectionPlayingNowIdentifier: String = "posterCellTable"
        static let tableSectionPlayingNowIdentifier: String = "playingNowCellTable"
        static let tableSectionMostPopularIdentifier: String = "mostPopularCellTable"
        static let moviesTitle: String = "MOVIEBOX"
    }
}
