//
//  ErrorViewStrings.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 14/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation

extension StringConstants {
    struct ErrorView {
        static let title: String = "Ops, We got an Error"
        static let description: String = "We were trying to get Movies data but something was wrong, please try again or contact us by email, Juandavidl2011.jdll@gmail.com"
        static let buttonTitle: String = "Try Again"
    }
}
