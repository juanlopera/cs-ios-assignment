//
//  ErrorView.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 14/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

final class ErrorView: UIView {
    
    // MARK: - TypeAlias
    typealias strings = StringConstants
    
    // MARK: - Private UI Properties
    private var imageView: UIImageView = {
        let view: UIImageView = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private var titleLabel: UILabel = {
        let label: UILabel = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = ColorsConstants.graySilver
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    private var subtitleLabel: UILabel = {
        let label: UILabel = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = ColorsConstants.graySilver
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 17)
        return label
    }()
    
    private(set) var actionButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(strings.ErrorView.buttonTitle, for: .normal)
        button.setTitleColor(ColorsConstants.graySilver, for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.layer.borderWidth = 1.5
        button.layer.borderColor = ColorsConstants.graySilver.cgColor
        button.layer.cornerRadius = 7
        return button
    }()
    
    // MARK: - Private Properties
    private let type: ErrorViewType
    
    // MARK: - Internal Properties
    var reloadAction: (() -> Void)? = nil {
        didSet {
            actionButton.isHidden = false
        }
    }
    
    // MARK: - Internal Init
    init(type: ErrorViewType) {
        self.type = type
        super.init(frame: .zero)
        setupViews()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: ViewCode
extension ErrorView: ViewConfigurationProtocol {
    func setupViewHierarchy() {
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        addSubview(actionButton)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: 100.0),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 0.5),
            
            titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 30),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            
            subtitleLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 15),
            subtitleLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            
            actionButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            actionButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            actionButton.heightAnchor.constraint(equalToConstant: 48),
            actionButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -52)
        ])
    }
    
    func configureViews() {
        backgroundColor = ColorsConstants.blackMineShaft
        imageView.image = type.image
        titleLabel.text = type.title
        subtitleLabel.text = type.subtitle
    }
}

// MARK: - Private Function
extension ErrorView {
    @objc
    private func buttonAction() {
        reloadAction?()
        removeFromSuperview()
    }
}
