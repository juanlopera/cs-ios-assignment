//
//  ErrorViewType.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 14/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

enum ErrorViewType {
    case service
    
    var title: String {
        return StringConstants.ErrorView.title
    }
    
    var subtitle: String {
        return StringConstants.ErrorView.description
    }
    
    var image: UIImage? {
        return UIImage(named: "icoConnectionError")
    }
}

