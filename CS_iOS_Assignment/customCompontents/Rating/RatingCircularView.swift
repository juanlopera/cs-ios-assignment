//
//  RatingCircularView.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 12/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

final class RatingCircularView: UIView {
    
    // MARK: - Private Properties
    private var progressLayer: CAShapeLayer
    private var trackLayer: CAShapeLayer
    private var progressColor: UIColor
    private var trackColor: UIColor
    private var ratingAverage: Float
    
    // MARK: - Internal Init
    init(frame: CGRect,
         progressColor: UIColor,
         trackColor: UIColor,
         ratingAverage: Float) {
        self.progressColor = progressColor
        self.trackColor = trackColor
        self.ratingAverage = ratingAverage
        progressLayer = CAShapeLayer()
        trackLayer = CAShapeLayer()
        super.init(frame: frame)
        createCircularPath()
        createAverageLabel()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Internal Functions
extension RatingCircularView {
    func setRatingAnimation(with duration: TimeInterval, and valueTrack: CGFloat) {
        let animation: CABasicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        animation.duration = duration
        animation.fromValue = 0
        animation.toValue = valueTrack
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        progressLayer.strokeEnd = valueTrack
        progressLayer.add(animation, forKey: "ratingAnimation")
    }
    
    func setStrokeEnd(valueTrack: CGFloat) {
        progressLayer.strokeEnd = valueTrack
    }
    
    func setNew(progressColor: UIColor, tarckColor: UIColor) {
        self.progressColor = progressColor
        self.trackColor = tarckColor
    }
}

// MARK: - Private Functions
extension RatingCircularView {
    private func createCircularPath() {
        backgroundColor = .clear
        layer.cornerRadius = self.frame.size.width / 2
        let circleArcCenter: CGPoint = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        let circleRadius: CGFloat = (frame.size.width - 1.5) / 2
        let circleStartAngle: CGFloat = CGFloat(-0.5 * .pi)
        let circleEndAngle: CGFloat = CGFloat(1.5 * .pi)
        let circleBeizerPath: UIBezierPath = UIBezierPath(arcCenter: circleArcCenter,
                                                          radius: circleRadius,
                                                          startAngle: circleStartAngle,
                                                          endAngle: circleEndAngle,
                                                          clockwise: true)
        setupTrackLayer(with: circleBeizerPath)
        setupProgressLayer(with: circleBeizerPath)
    }
    
    private func setupTrackLayer(with circlePath: UIBezierPath) {
        trackLayer.path = circlePath.cgPath
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.strokeColor = trackColor.cgColor
        trackLayer.lineWidth = 5.0
        trackLayer.strokeEnd = 1.0
        layer.addSublayer(trackLayer)
    }
    
    private func setupProgressLayer(with circlePath: UIBezierPath) {
        progressLayer.path = circlePath.cgPath
        progressLayer.fillColor = UIColor.clear.cgColor
        progressLayer.strokeColor = progressColor.cgColor
        progressLayer.lineWidth = 5.0
        progressLayer.strokeEnd = 0.0
        layer.addSublayer(progressLayer)
    }
    
    private func createAverageLabel() {
        let averageLabelFrame: CGRect = CGRect(x: 10, y: 10, width: self.frame.width, height: 20)
        let averageLabel: UILabel = UILabel(frame: averageLabelFrame)
        averageLabel.font = UIFont.boldSystemFont(ofSize: 15)
        averageLabel.textColor = ColorsConstants.graySilver
        averageLabel.text = getAvarageString()
        addSubview(averageLabel)
    }
    
    private func getAvarageString() -> String {
        let avarageInt: Int = Int(ratingAverage * 10)
        return String(avarageInt)
    }
}
