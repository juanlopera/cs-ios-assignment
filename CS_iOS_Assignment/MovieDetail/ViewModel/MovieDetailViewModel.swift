//
//  MovieDetailViewModel.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

final class MovieDetailViewModel {
    
    // MARK: - Private Properties
    private let movieId: Int
    private let api: MovieDetailAPIProtocol
    private var movieDetail: MovieDetail?
    
    // MARK: - Internal Properties
    var currentCellIndex: Int = 0
    
    // MARK: - Delegate
    weak var delegate: MovieDetailViewControllerDelegate?
    
    // MARK: - Internal Init
    init(movieId: Int,
         api: MovieDetailAPIProtocol = MovieDetailAPI()) {
        self.movieId = movieId
        self.api = api
    }
    
    // MARK: - Computed Properties
    var principalImage: UIImage {
        guard
            let movieDetail: MovieDetail = movieDetail
        else {
            return UIImage(named: "moviePlaceholder") ?? UIImage()
        }
        return UIImage.getMovieUIImageFrom(posterPath: movieDetail.posterPath)
    }
    
    var movieName: String {
        guard
            let movieDetail: MovieDetail = movieDetail
        else {
            return "Not Information"
        }
        return movieDetail.originalTitle
    }
    
    var releaseDateAndDuration: String {
        guard
            let movieDetail: MovieDetail = movieDetail
        else {
            return "Not Information"
        }
        return "\(movieDetail.releaseDate.getReleaseDate()) - \(getRuntime(with: movieDetail))"
    }
    
    var overviewTitle: String {
        return StringConstants.MovieDetail.overviewTitle
    }
    
    var overview: String {
        guard
            let movieDetail: MovieDetail = movieDetail
        else {
            return "Not Information"
        }
        return movieDetail.overview
    }
    
    var genres: [String] {
        guard
            let movieDetail: MovieDetail = movieDetail
        else {
            return [String]()
        }
        var genresArray: [String] = [String]()
        movieDetail.genres.forEach { genre in
            genresArray.append(genre.name)
        }
        return genresArray
    }
    
    var genre: String {
        guard
            let movieDetail: MovieDetail = movieDetail,
            movieDetail.genres.indices.contains(currentCellIndex)
        else {
            return ""
        }
        return movieDetail.genres[currentCellIndex].name
    }
}

// MARK: - Internal Function
extension MovieDetailViewModel {
    func fetchMovieDetail() {
        api.getMovieDetail(with: movieId) { [weak self] result in
            switch result {
            case .success(let movieDetail):
                self?.movieDetail = movieDetail
                self?.delegate?.loadInformation()
            case .failure(_):
                self?.delegate?.serviceError()
            }
        }
    }
    
    func getCellSizeAt(index: Int) -> CGFloat {
        guard
            let movieDetail: MovieDetail = movieDetail,
            movieDetail.genres.indices.contains(index)
        else {
            return 0.0
        }
        let genreSize: CGFloat = CGFloat(movieDetail.genres[index].name.count)
        return genreSize * 11.0
    }
}

// MARK: - Private Function
extension MovieDetailViewModel {
    private func getRuntime(with movieDetail: MovieDetail) -> String {
        var minutes: Int = movieDetail.runtime
        var hours: Int = 0
        while minutes > 60 {
            hours += 1
            minutes -= 60
        }
        return "\(hours)h \(minutes)m"
    }
}
