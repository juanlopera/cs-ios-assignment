//
//  MovieDetailAPI.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation

protocol MovieDetailAPIProtocol {
    func getMovieDetail(with id: Int, completionHandler: @escaping(Result<MovieDetail, HttpError>) -> Void)
}

final class MovieDetailAPI: MovieDetailAPIProtocol {
    
    // MARK: - Private Properties
    private let apiKey: String
    private let baseURL: String
    private let requestManager: RequestManager
    
    // MARK: - Internal Init
    init(apiKey: String = "55957fcf3ba81b137f8fc01ac5a31fb5",
         baseURL: String = "https://api.themoviedb.org/3/movie/",
         requestManager: RequestManager = RequestManager()) {
        self.apiKey = apiKey
        self.baseURL = baseURL
        self.requestManager = requestManager
    }
    
    func getMovieDetail(with id: Int, completionHandler: @escaping (Result<MovieDetail, HttpError>) -> Void) {
        let securityScheme: String  = "https"
        let host: String = "api.themoviedb.org"
        let path: String = "/3/movie/\(id)"
        let queryItems: [URLQueryItem] = [URLQueryItem(name: "language", value: "en-US"),
                                          URLQueryItem(name: "api_key", value: apiKey)]
        requestManager.request(with: securityScheme, host: host, path: path, queryItems: queryItems, completion: completionHandler)
    }
}
