//
//  MovieDetailViewController.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

protocol MovieDetailViewControllerDelegate: AnyObject {
    func loadInformation()
    func serviceError()
}

final class MovieDetailViewController: UIViewController {
    
    // MARK: - Private UI Properties
    private let baseView: MovieDetailBaseView = MovieDetailBaseView()
    
    // MARK: - Private Properties
    private let coordinator: MovieCoordinator
    private let viewModel: MovieDetailViewModel
    
    // MARK: - Internal Init
    init(coordinator: MovieCoordinator,
         viewModel: MovieDetailViewModel) {
        self.coordinator = coordinator
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    override func loadView() {
        view = baseView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        baseView.delegate = self
        baseView.isLoading(true)
        viewModel.fetchMovieDetail()
    }
}

// MARK: - MovieDetailViewControl Delegate Implementation
extension MovieDetailViewController: MovieDetailViewControllerDelegate {
    func loadInformation() {
        DispatchQueue.main.async { [self] in
            baseView.isLoading(false)
            baseView.set(viewModel: viewModel)
        }
    }
    
    func serviceError() {
        DispatchQueue.main.async { [self] in
            showError(with: .service) {
                viewModel.fetchMovieDetail()
            }
        }
    }
}

// MARK: - BaseView Protocol
extension MovieDetailViewController: MovieDetailBaseViewDelegate {
    func didTapCloseButton() {
        coordinator.back()
    }
}
