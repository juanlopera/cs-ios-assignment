//
//  DatasourceTagGenreCollectionView.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 14/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

final class DatasourceTagGenreCollectionView: NSObject, UICollectionViewDataSource {
    
    // MARK: - TypeAlias
    typealias strings = StringConstants
    
    //MARK: - Internal Properties
    var viewModel: MovieDetailViewModel?
    
    // MARK: - Datasource Protocol Implementation
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.genres.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        viewModel?.currentCellIndex = indexPath.item
        guard
            let viewModel: MovieDetailViewModel = viewModel
        else {
            return UICollectionViewCell()
        }
        guard
            let cell: TagGenreCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: strings.MovieDetail.tagGenreCellIdentifier, for: indexPath) as? TagGenreCollectionCell
        else {
            return UICollectionViewCell()
        }
        cell.set(viewModel: viewModel)
        return cell
    }
}
