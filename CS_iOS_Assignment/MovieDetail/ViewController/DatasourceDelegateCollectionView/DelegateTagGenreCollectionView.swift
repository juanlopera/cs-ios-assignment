//
//  DelegateTagGenreCollectionView.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 14/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

final class DelegateTagGenreCollectionView: NSObject, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    //MARK: - Internal Properties
    var viewModel: MovieDetailViewModel?
    
    // MARK: - Delegate CollectionView Implementation
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = viewModel?.getCellSizeAt(index: indexPath.item) ?? 0.0
        return CGSize(width: width, height: 50.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}
