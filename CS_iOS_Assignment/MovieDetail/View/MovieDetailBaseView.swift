//
//  MovieDetailBaseView.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Lottie
import UIKit

protocol MovieDetailBaseViewDelegate: AnyObject {
    func didTapCloseButton()
}

final class MovieDetailBaseView: UIView {
    
    // MARK: - Private UI Properties
    private let backgroundView: UIView = {
        let view: UIView = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = ColorsConstants.blackMineShaft
        view.alpha = 0.7
        return view
    }()
    private let principalImageView: UIImageView = {
        let imageView: UIImageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.layer.borderWidth = 1.0
        imageView.layer.borderColor = ColorsConstants.graySilver.cgColor
        return imageView
    }()
    
    private let movieNameLabel: UILabel = {
        let label: UILabel = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = ColorsConstants.graySilver
        label.textAlignment = .center
        return label
    }()
    
    private let releaseAndDurationMovieLabel: UILabel = {
        let label: UILabel = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = ColorsConstants.graySilver
        label.textAlignment = .center
        return label
    }()
    
    private let overviewTitleLabel: UILabel = {
        let label: UILabel = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.textColor = ColorsConstants.graySilver
        label.textAlignment = .left
        return label
    }()
    
    private let overviewLabel: UILabel = {
        let label: UILabel = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = ColorsConstants.graySilver
        label.numberOfLines = 0
        return label
    }()
    
    private let closeButton: UIButton = {
        let button: UIButton = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("X", for: .normal)
        button.setTitleColor(ColorsConstants.graySilver, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 30)
        button.addTarget(self, action: #selector(closeAction), for: .touchDown)
        return button
    }()
    
    private let tagGenreCollectionView: UICollectionView = {
        let flowLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 10
        let collectionView: UICollectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.register(TagGenreCollectionCell.self, forCellWithReuseIdentifier: StringConstants.MovieDetail.tagGenreCellIdentifier)
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    private let animationView: AnimationView = {
        let animationView: AnimationView = .init(name: "movieAnimation")
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationView.loopMode = .loop
        animationView.animationSpeed = 0.5
        animationView.isHidden = true
        return animationView
    }()
    
    // MARK: - Private Properties
    // MARK: - Private Properties
    private var datasourceCollection: DatasourceTagGenreCollectionView = DatasourceTagGenreCollectionView()
    private var delegateCollection: DelegateTagGenreCollectionView = DelegateTagGenreCollectionView()
    
    // MARK: - Delegate
    weak var delegate: MovieDetailBaseViewDelegate?
    
    // MARK: - Internal Init
    init() {
        super.init(frame: .zero)
        setupViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewCode
extension MovieDetailBaseView: ViewConfigurationProtocol {
    func setupViewHierarchy() {
        [backgroundView, closeButton, principalImageView, movieNameLabel, releaseAndDurationMovieLabel, overviewTitleLabel, overviewLabel, tagGenreCollectionView, animationView].forEach { view in
            addSubview(view)
        }
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            backgroundView.topAnchor.constraint(equalTo: self.topAnchor),
            backgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            backgroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            backgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            closeButton.topAnchor.constraint(equalTo: self.topAnchor, constant: 30),
            closeButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            closeButton.widthAnchor.constraint(equalToConstant: 70),
            closeButton.heightAnchor.constraint(equalToConstant: 60),
            
            principalImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 90),
            principalImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 125),
            principalImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -125),
            principalImageView.heightAnchor.constraint(equalToConstant: 200),
            
            movieNameLabel.topAnchor.constraint(equalTo: principalImageView.bottomAnchor, constant: 10),
            movieNameLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            movieNameLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            
            releaseAndDurationMovieLabel.topAnchor.constraint(equalTo: movieNameLabel.bottomAnchor, constant: 5),
            releaseAndDurationMovieLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            releaseAndDurationMovieLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            
            overviewTitleLabel.topAnchor.constraint(equalTo: releaseAndDurationMovieLabel.bottomAnchor, constant: 30),
            overviewTitleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 40),
            overviewTitleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -40),
            
            overviewLabel.topAnchor.constraint(equalTo: overviewTitleLabel.bottomAnchor, constant: 30),
            overviewLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 40),
            overviewLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -40),
            
            tagGenreCollectionView.topAnchor.constraint(equalTo: overviewLabel.bottomAnchor, constant: 10),
            tagGenreCollectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 40),
            tagGenreCollectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -40),
            tagGenreCollectionView.heightAnchor.constraint(equalToConstant: 50.0),
            
            animationView.topAnchor.constraint(equalTo: self.topAnchor),
            animationView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            animationView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            animationView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
}

// MARK: - Internal Function
extension MovieDetailBaseView {
    func set(viewModel: MovieDetailViewModel) {
        DispatchQueue.main.async { [self] in
            datasourceCollection.viewModel = viewModel
            delegateCollection.viewModel = viewModel
            tagGenreCollectionView.dataSource = datasourceCollection
            tagGenreCollectionView.delegate = delegateCollection
            principalImageView.image = viewModel.principalImage
            movieNameLabel.text = viewModel.movieName
            releaseAndDurationMovieLabel.text = viewModel.releaseDateAndDuration
            overviewTitleLabel.text = viewModel.overviewTitle
            overviewLabel.text = viewModel.overview
            tagGenreCollectionView.reloadData()
        }
    }
    
    func isLoading(_ value: Bool) {
        DispatchQueue.main.async { [self] in
            if value {
                animationView.isHidden = false
                animationView.play()
            } else {
                animationView.isHidden = true
                animationView.stop()
            }
        }
    }
}

// MARK: - Private Function
extension MovieDetailBaseView {
    @objc
    private func closeAction() {
        delegate?.didTapCloseButton()
    }
}
