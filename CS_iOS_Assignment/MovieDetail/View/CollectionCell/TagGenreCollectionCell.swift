//
//  TagGenreCollectionCell.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 14/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

final class TagGenreCollectionCell: UICollectionViewCell {
    
    // MARK: - Private UI Properties
    private let genreLabel: UILabel = {
        let label: UILabel = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = ColorsConstants.blackMineShaft
        label.font = UIFont.systemFont(ofSize: 15)
        label.textAlignment = .center
        return label
    }()
    
    private let genreView: UIView = {
        let view: UIView = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = ColorsConstants.graySilver
        view.layer.cornerRadius = 3
        return view
    }()
    
    // MARK: - Internal Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewCode
extension TagGenreCollectionCell: ViewConfigurationProtocol {
    func setupViewHierarchy() {
        contentView.addSubview(genreView)
        contentView.addSubview(genreLabel)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            genreLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            genreLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            genreLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -2)
        ])
    }
    
    func configureViews() {
        contentView.backgroundColor = .clear
    }
}

// MARK: - Internal Function
extension TagGenreCollectionCell {
    func set(viewModel: MovieDetailViewModel) {
        genreLabel.text = viewModel.genre
        setupViewAroundLabel()
    }
}

// MARK: - Private Function
extension TagGenreCollectionCell {
    private func setupViewAroundLabel() {
        NSLayoutConstraint.activate([
            genreView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            genreView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 2),
            genreView.trailingAnchor.constraint(equalTo: genreLabel.trailingAnchor, constant: 6),
            genreView.heightAnchor.constraint(equalToConstant: 25.0)
        ])
    }
}
