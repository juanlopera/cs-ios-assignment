//
//  MovieDetailStrings.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation

extension StringConstants {
    struct MovieDetail {
        static let overviewTitle: String  = "Overview"
        static let tagGenreCellIdentifier: String  = "GenreCell"
    }
}
