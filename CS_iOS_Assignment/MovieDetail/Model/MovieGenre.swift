//
//  MovieGenre.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation

struct MovieGenre: Codable {
    let id: Int
    let name: String
}
