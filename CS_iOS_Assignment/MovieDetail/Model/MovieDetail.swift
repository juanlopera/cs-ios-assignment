//
//  MovieDetail.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation

struct MovieDetail: Codable {
    let genres: [MovieGenre]
    let id: Int
    let originalTitle: String
    let overview: String
    let posterPath: String
    let releaseDate: String
    let runtime: Int

    enum CodingKeys: String, CodingKey {
        case genres
        case id
        case originalTitle = "original_title"
        case overview
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case runtime
    }
}
