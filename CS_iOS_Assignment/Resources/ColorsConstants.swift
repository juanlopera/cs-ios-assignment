//
//  ColorsConstants.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 12/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

struct ColorsConstants {
    static let yellowCreamCan: UIColor = #colorLiteral(red: 0.9881920218, green: 0.8171316385, blue: 0.3197386861, alpha: 1)
    static let blackMineShaft: UIColor = #colorLiteral(red: 0.1293945909, green: 0.1294237971, blue: 0.1293907464, alpha: 1)
    static let greenEmerald: UIColor = #colorLiteral(red: 0.1131517813, green: 0.8158859015, blue: 0.4785249829, alpha: 1)
    static let grayTundora: UIColor = #colorLiteral(red: 0.2509519458, green: 0.251000315, blue: 0.2509455681, alpha: 1)
    static let graySilver: UIColor = #colorLiteral(red: 0.7999204993, green: 0.8000556827, blue: 0.7999026775, alpha: 1)
    static let greenEverglade: UIColor = #colorLiteral(red: 0.1254901961, green: 0.2705882353, blue: 0.1568627451, alpha: 1)
    static let yellowWaiouru: UIColor = #colorLiteral(red: 0.2509803922, green: 0.2431372549, blue: 0.05490196078, alpha: 1)
}
