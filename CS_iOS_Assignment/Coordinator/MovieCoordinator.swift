//
//  MovieCoordinator.swift
//  CS_iOS_Assignment
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import UIKit

final class MovieCoordinator {
    
    // MARK: - Private Properties
    private var navigationController: UINavigationController
    
    // MARK: - Internal Init
    init(navigationController: UINavigationController = UINavigationController()) {
        self.navigationController = navigationController
    }
    
    func start() -> UINavigationController {
        let initialViewController: MovieListViewController = MovieListViewController(coordinator: self)
        navigationController.setViewControllers([initialViewController], animated: true)
        return navigationController
    }
    
    func showDetailt(with movieId: Int) {
        let viewModel: MovieDetailViewModel = MovieDetailViewModel(movieId: movieId)
        let viewController: MovieDetailViewController = MovieDetailViewController(coordinator: self,
                                                                                  viewModel: viewModel)
        viewController.modalPresentationStyle = .fullScreen
        viewController.modalTransitionStyle = .crossDissolve
        navigationController.present(viewController, animated: true, completion: nil)
    }
    
    func back() {
        navigationController.dismiss(animated: true, completion: nil)
    }
}
