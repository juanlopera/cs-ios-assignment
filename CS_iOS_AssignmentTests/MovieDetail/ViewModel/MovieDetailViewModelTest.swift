//
//  MovieDetailViewModelTest.swift
//  CS_iOS_AssignmentTests
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import XCTest
@testable import CS_iOS_Assignment

final class MovieDetailViewModelTest: XCTestCase {
    
    // MARK: - Testable Properties
    var viewModel: MovieDetailViewModel!
    var api: MovieDetailAPIMock!
    
    override func setUp() {
        api = MovieDetailAPIMock(shouldFail: false)
        viewModel = MovieDetailViewModel(movieId: 464052, api: api)
    }

    override func tearDown() {
        api = nil
        viewModel = nil
    }
    
    func testMovieDetailPropertyIsNil() {
        XCTAssertTrue(viewModel.movieName == "Not Information")
    }
    
    func testMovieDetailPropertyIsNotNil() {
        viewModel.fetchMovieDetail()
        XCTAssertTrue(viewModel.movieName == "Wonder Woman 1984")
    }
    
    func testReleaseDateAndDurationComputedPropertyWithValueDecember162020() {
        viewModel.fetchMovieDetail()
        XCTAssertTrue(viewModel.releaseDateAndDuration == "December 16, 2020 - 2h 31m")
    }
    
    func testOverviewTitleComputedPropertyWithValueOverview() {
        viewModel.fetchMovieDetail()
        XCTAssertTrue(viewModel.overviewTitle == "Overview")
    }
    
    func testOverviewComputedPropertyWithValue() {
        viewModel.fetchMovieDetail()
        XCTAssertTrue(viewModel.overview == "Wonder Woman comes into conflict with the Soviet Union during the Cold War in the 1980s and finds a formidable foe by the name of the Cheetah.")
    }
    
    func testGenresComputedPropertyInZeroPositionWithValueAction() {
        viewModel.fetchMovieDetail()
        XCTAssertTrue(viewModel.genres[0] == "Action")
    }
    
    func testGenresComputedPropertyInOnePositionWithValueAdventure() {
        viewModel.fetchMovieDetail()
        XCTAssertTrue(viewModel.genres[1] == "Adventure")
    }
    
    func testGenreComputedPropertyInOnePositionWithValueAdventure() {
        viewModel.fetchMovieDetail()
        viewModel.currentCellIndex = 1
        XCTAssertTrue(viewModel.genre == "Adventure")
    }
    
    func testCellSizeWithIndexOne() {
        viewModel.fetchMovieDetail()
        viewModel.currentCellIndex = 1
        XCTAssertTrue(viewModel.getCellSizeAt(index: 1) == CGFloat(viewModel.genre.count) * 11.0)
    }
}
