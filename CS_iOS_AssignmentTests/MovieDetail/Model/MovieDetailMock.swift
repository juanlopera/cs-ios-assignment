//
//  MovieDetailMock.swift
//  CS_iOS_AssignmentTests
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

@testable import CS_iOS_Assignment

extension MovieDetail {
    static func createMock() -> MovieDetail {
        return MovieDetail(genres: MovieGenre.createArrayMock(),
                           id: 464052,
                           originalTitle: "Wonder Woman 1984",
                           overview: "Wonder Woman comes into conflict with the Soviet Union during the Cold War in the 1980s and finds a formidable foe by the name of the Cheetah.",
                           posterPath: "/8UlWHLMpgZm9bx6QYh0NFoq67TZ.jpg",
                           releaseDate: "2020-12-16",
                           runtime: 151)
    }
}
