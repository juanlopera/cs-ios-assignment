//
//  MovieGenreMock.swift
//  CS_iOS_AssignmentTests
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

@testable import CS_iOS_Assignment

extension MovieGenre {
    static func createArrayMock() -> [MovieGenre] {
        let mockOne: MovieGenre = MovieGenre(id: 28, name: "Action")
        let mockTwo: MovieGenre = MovieGenre(id: 12, name: "Adventure")
        return [mockOne, mockTwo]
    }
}
