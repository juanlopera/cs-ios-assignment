//
//  MovieListAPIMock.swift
//  CS_iOS_AssignmentTests
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

@testable import CS_iOS_Assignment

final class MovieListAPIMock: MoviesListAPIProtocol {
    
    // MARK: Private Properties
    private let shouldFail: Bool
    
    // MARK: Internal Init
    init(shouldFail: Bool = false) {
        self.shouldFail = shouldFail
    }
    
    func fetchMoviesPlayingNow(completionHandler: @escaping (Result<Movie, HttpError>) -> Void) {
        if shouldFail {
            completionHandler(.failure(.badRequest))
        } else {
            completionHandler(.success(Movie.createMock()))
        }
    }
    
    func fetchMoviesMostPopular(page: String, completionHandler: @escaping (Result<Movie, HttpError>) -> Void) {
        if shouldFail {
            completionHandler(.failure(.badRequest))
        } else {
            completionHandler(.success(Movie.createMock()))
        }
    }
}
