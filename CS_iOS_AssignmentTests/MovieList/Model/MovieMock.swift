//
//  MovieMock.swift
//  CS_iOS_AssignmentTests
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

@testable import CS_iOS_Assignment

extension Movie {
    static func createMock() -> Movie {
        return Movie(dates: MovieDates.createMock(),
                                 page: 1,
                                 results: MovieResult.createArrayMock(),
                                 totalPages: 1,
                                 totalResults: 2)
    }
}
