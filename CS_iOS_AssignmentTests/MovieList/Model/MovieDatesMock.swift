//
//  MovieDatesMock.swift
//  CS_iOS_AssignmentTests
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

@testable import CS_iOS_Assignment

extension MovieDates {
    static func createMock() -> MovieDates {
        return MovieDates(maximum: "2021-02-18", minimum: "2021-01-01")
    }
}
