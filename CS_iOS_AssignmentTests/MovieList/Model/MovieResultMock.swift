//
//  MovieResultMock.swift
//  CS_iOS_AssignmentTests
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

@testable import CS_iOS_Assignment

extension MovieResult {
    static func createArrayMock() -> [MovieResult] {
        let resultOne: MovieResult = MovieResult(id: 464052,
                                                 originalTitle: "Wonder Woman 1984",
                                                 posterPath: "/8UlWHLMpgZm9bx6QYh0NFoq67TZ.jpg",
                                                 releaseDate: "2020-12-16",
                                                 title: "Wonder Woman 1984",
                                                 voteAverage: 7)
        let resultTwo: MovieResult = MovieResult(id: 602269,
                                                 originalTitle: "The Little Things",
                                                 posterPath: "/c7VlGCCgM9GZivKSzBgzuOVxQn7.jpg",
                                                 releaseDate: "2021-01-27",
                                                 title: "The Little Things",
                                                 voteAverage: 5)
        return [resultOne, resultTwo]
    }
}
