//
//  MoviesListViewModelTest.swift
//  CS_iOS_AssignmentTests
//
//  Created by Juan David Lopera  on 13/2/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import XCTest
@testable import CS_iOS_Assignment

final class MoviesListViewModelTest: XCTestCase {
    
    // MARK: - Testable Properties
    var viewModel: MoviesListViewModel!
    var api: MovieListAPIMock!
    
    override func setUp() {
        api = MovieListAPIMock(shouldFail: false)
        viewModel = MoviesListViewModel(api: api)
    }

    override func tearDown() {
        api = nil
        viewModel = nil
    }
    
    func testPlayingNowHeightRow200() {
        XCTAssertTrue(viewModel.playingNowHeightRow == 200.0)
    }
    
    func testMostPopularHeightRow130() {
        XCTAssertTrue(viewModel.mostPopularHeightRow == 130.0)
    }
    
    func testFuncFetchMovieListFillMovieSectionObject() {
        viewModel.fetchMovieList()
        XCTAssertTrue(viewModel.numberOfSection > 0)
    }
    
    func testFuncFetchMovieListFillMovieSectionWithTwoSection() {
        viewModel.fetchMovieList()
        XCTAssertTrue(viewModel.numberOfSection == 2)
    }
    
    func testNumberOfRowInSectionTableWithSectionZeroMustBeOneRow() {
        viewModel.fetchMovieList()
        viewModel.currentSection = 0
        XCTAssertTrue(viewModel.numberOfRowInSectionTable == 1)
    }
    
    func testNumberOfRowInSectionTableWithSectionOneMustBeTwoRow() {
        viewModel.fetchMovieList()
        viewModel.currentSection = 1
        XCTAssertTrue(viewModel.numberOfRowInSectionTable == 2)
    }
    
    func testSectionPropertieWhenSectionIsEqualToZeroItMustBePlayingNowSection() {
        viewModel.fetchMovieList()
        viewModel.currentSection = 0
        XCTAssertTrue(viewModel.section == .playingNow)
    }
    
    func testSectionPropertieWhenSectionIsEqualToOneItMustBeMostPopularSection() {
        viewModel.fetchMovieList()
        viewModel.currentSection = 1
        XCTAssertTrue(viewModel.section == .mostPopular)
    }
    
    func testMovieNameComputedPropertyWhenSectionIsEqualToZeroAndRowEqualToZeroItMustBeWonderWoman1984() {
        viewModel.fetchMovieList()
        viewModel.currentSection = 0
        viewModel.currentTableCellIndex = 0
        XCTAssertTrue(viewModel.movieName == "Wonder Woman 1984")
    }
    
    func testMovieNameComputedPropertyWhenSectionIsEqualToZeroAndRowEqualToOneItMustBeTheLittleThings() {
        viewModel.fetchMovieList()
        viewModel.currentSection = 0
        viewModel.currentTableCellIndex = 1
        XCTAssertTrue(viewModel.movieName == "The Little Things")
    }
    
    func testMovieNameComputedPropertyWhenSectionIsEqualToOneAndRowEqualToZeroItMustBeWonderWoman1984() {
        viewModel.fetchMovieList()
        viewModel.currentSection = 1
        viewModel.currentTableCellIndex = 0
        XCTAssertTrue(viewModel.movieName == "Wonder Woman 1984")
    }
    
    func testMovieNameComputedPropertyWhenSectionIsEqualToOneAndRowEqualToOneItMustBeTheLittleThings() {
        viewModel.fetchMovieList()
        viewModel.currentSection = 1
        viewModel.currentTableCellIndex = 1
        XCTAssertTrue(viewModel.movieName == "The Little Things")
    }
    
    func testMovieReleaseDateComputedPropertyWhenSectionIsEqualToZeroAndRowEqualToZeroItMustBeEmpty() {
        viewModel.fetchMovieList()
        viewModel.currentSection = 0
        viewModel.currentTableCellIndex = 0
        XCTAssertTrue(viewModel.movieReleaseDate == "")
    }
    
    func testMovieReleaseDateComputedPropertyWhenSectionIsEqualToZeroAndRowEqualToOneItMustBeEmpty() {
        viewModel.fetchMovieList()
        viewModel.currentSection = 0
        viewModel.currentTableCellIndex = 1
        XCTAssertTrue(viewModel.movieReleaseDate == "")
    }
    
    func testMovieReleaseDateComputedPropertyWhenSectionIsEqualToOneAndRowEqualToZeroItMustBeDecember162020() {
        viewModel.fetchMovieList()
        viewModel.currentSection = 1
        viewModel.currentTableCellIndex = 0
        XCTAssertTrue(viewModel.movieReleaseDate == "December 16, 2020")
    }
    
    func testMovieReleaseDateComputedPropertyWhenSectionIsEqualToOneAndRowEqualToOneItMustBeJanuary272021() {
        viewModel.fetchMovieList()
        viewModel.currentSection = 1
        viewModel.currentTableCellIndex = 1
        XCTAssertTrue(viewModel.movieReleaseDate == "January 27, 2021")
    }
    
    func testMovieRatingComputedPropertyWhenSectionIsEqualToOneAndRowEqualToZeroItAnimationPropertyMustBeTrue() {
        viewModel.fetchMovieList()
        viewModel.currentSection = 1
        viewModel.currentTableCellIndex = 0
        XCTAssertTrue(viewModel.movieRating.animation == true)
    }
    
    func testMovieRatingComputedPropertyWhenSectionIsEqualToOneAndRowEqualToZeroItRatingPropertyMustBe7() {
        viewModel.fetchMovieList()
        viewModel.currentSection = 1
        viewModel.currentTableCellIndex = 0
        XCTAssertTrue(viewModel.movieRating.rating == 7)
    }
    
    func testMovieRatingComputedPropertyWhenSectionIsEqualToOneAndRowEqualToOneItAnimationPropertyMustBeTrue() {
        viewModel.fetchMovieList()
        viewModel.currentSection = 1
        viewModel.currentTableCellIndex = 1
        XCTAssertTrue(viewModel.movieRating.animation == true)
    }
    
    func testMovieRatingComputedPropertyWhenSectionIsEqualToOneAndRowEqualToOneItRatingPropertyMustBe7() {
        viewModel.fetchMovieList()
        viewModel.currentSection = 1
        viewModel.currentTableCellIndex = 1
        XCTAssertTrue(viewModel.movieRating.rating == 5)
    }

    func testDidRatingAnimationWhenSectionIsEqualToOneAndRowEqualToOneItMustChangeAnimateFromTrueToFalse() {
        viewModel.fetchMovieList()
        viewModel.currentSection = 1
        viewModel.currentTableCellIndex = 1
        viewModel.didRatingAnimation()
        XCTAssertTrue(viewModel.movieRating.animation == false)
    }
    
    func testFunctionGetSectionTitleWhenSectionIsEqualToZeroItMustBePlayingNowTitle() {
        viewModel.fetchMovieList()
        XCTAssertTrue(viewModel.getSectionTitle(section: 0) == "Playing now")
    }
    
    func testFunctionGetSectionTitleWhenSectionIsEqualToOneItMustBeMostPopularTitle() {
        viewModel.fetchMovieList()
        XCTAssertTrue(viewModel.getSectionTitle(section: 1) == "Most Popular")
    }
}
