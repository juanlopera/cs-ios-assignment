# Juan David Lopera Lopez
## Email: juan.lopez@bringglobal.com, juandavidl2011.jdll@gmail.com
# Strategy
## Architecutre
I choosed the decision to create everything with **MVVM** architecture design pattern, I think that is the best architecture design pattern, even apple is trying to put it in his new SwiftUI

## View Code
I have created everything in ViewCode (Not storyboard), because I in my work experience, **[(1)]** I figure out that it create a lot of git (merge) conflict specially when you are working with more than 2 iOS developers in the same team, **[(2)]** Work with storyboard, it make difficult the codereview to check that everything was done with good code quality, Also many times with storyboard is difficult to reuse views/viewControllers/Components

But.... it doesn't means that I don't have really good skills working with storyboard, because I have it

## ViewConfigurationProtocol
I have created **ViewConfigurationProtocol** because in that way I can have the garantee that every base view will be build in the same way (steps), so I can have a build view pattern in my own code

## Request Manager
I have created a class called **RequestManager**.... Why?? .... Simple, Because I figure out that I needed make more than 1 request, so I didn't want to duplicate code in every request, it is not a good practice, so the soluciton was create a request manager than manage every request using Generics with constraint Decodable, so it class can manager the request and return a model that you provide with the information that came from the service 

## Handle Error / View Error
How we know, when we make a request we can have a possitive ou negative response, so when I got a negative response form the request I need to handle it... well... I have create in customCompontents folder a class called **ErrorView**, it view will be presented to user, user will notice that something was wrong and him will have 2 opcions, **1** Make again the request tapping Try again button, or **2** send a email telling about the problem
Can we have betters solutions?? yes, we can, but it was my soluction for a test jejejeje

## Dependencies? Pods? 
Well about this topic, I need to say that I don't like to use dependency when is not necessary, for example, many persons take the decision to install **Alamofire** pod because it handle your request and etc, etc.... the question is.... is it necessary? I my opinion not, because we can do the request with URLSession that is a native framework and we can have the same result
So I just install pods (create dependency) when I really need to do it, otherwise I prefer to user all the navite frameworks/components

## Loading View with animation
I wanted to show you that also I can use pods in my project, so my decision was install lottie pod to create a nice animation that will be presented to user while the App is making a request to get the movies information

## Constants
well, as we know, every project has constants, it constants can be **Strings**, **Colors**, **Assets** etc etc... For do it we have many options, One of my favorite is SwiftGen pod, it work perfect and is so useful.... but.... in this project I didn't see the need to install SwiftGen pod, so I have created two files, One of them for handle Strings constants and the other one for handle Colors constants, In that way I can have everything in the same place and If tomorrow someone tell me to change a specific color or Strings, so I just go the constant Strings or Colors file and make the change and it will be upload in all the app without the need to change in every single place that it was used

# iOS Assignment CS

This is a placeholder README file with the instructions for the assingment. We expect you to build your own README file.

## Instructions

You can find all the instrucrtions for the assingment on [Assingment Instructions](https://docs.google.com/document/d/1zCIIkybu5OkMOcsbuC106B92uqOb3L2PPo9DNFBjuWg/edit?usp=sharing).

## Delivering the code
* Fork this repo and select the access level as private **[Check how to do it here](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)**
* Go to settings and add the user **m-cs-recruitment@backbase.com** with the read access **[Check how to do it here](https://confluence.atlassian.com/bitbucket/grant-repository-access-to-users-and-groups-221449716.html)**
* Send an e-mail to **m-cs-recruitment@backbase.com** with your info and repo once the development is done

Please remember to work with small commits, it help us to see how you improve your code :)
